	.include "lorom.inc"
	.include "equates.inc"
	.include "65816.inc"

	.segment "CODE"

	.p816
	.a8
	.i16

	.zeropage

	;; A scratch pointer.
PtrTMP:
PtrTMP_Lo:	.byte 0
PtrTMP_Hi:	.byte 0

	;; Scratch byte.
Scratch:	.byte 0

	
	;; Pointer to the RAM copy of the OAM.
PtrOAM:
PtrOAM_Lo:	.byte 0
PtrOAM_Hi:	.byte 0

	.code

;;; ;;;;;;;;
;;; Vectors
;;; ;;;;;;;;
RESET_Vector:
	jmp	Reset

;;; VBL vector.
VBLANK_Vector:
	rti
	
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Reset:
	clc			; Native mode
	xce
	rep	#$10		; X/Y 16-bit
	sep	#$20		; A 8-bit

Start:
	;; force a VBLANK
	lda	#%10000000
	sta	INIDISP

	;; Set up the OAM
	ldx	#OAM_RAMCopy
	stx	PtrOAM
	jsr	InitializeOAM

	;;
	ldx	#$1000		; destination $2000
	stx	VMADDR		; store address
	lda	#$01		; source bank
	ldx	#$8000		; load from first tileset
	ldy	#$4000		; it's $4000 long
	jsr	LoadVRAM	; and load

	ldx	#$5000		; destination $A000
	stx	VMADDR		; store address
	lda	#$01		; source bank
	ldx	#$C000		; load from first tileset
	ldy	#$4000		; it's $4000 long
	jsr	LoadVRAM	; and load

	lda	#$00
	sta	BG1SC		; BG1 starts at $0000
	lda	#$04		
	sta	BG2SC		; BG2 starts at $0800
	lda	#$08
	sta	BG3SC		; BG3 starts at $1000

	lda	#$31		
	sta	BG12NBA		; BG1 tiles are at $2000, BG2 tiles at $6000
	lda	#$05		
	sta	BG34NBA		; BG3 tiles are at $A000
	
	jsr	LoadShooterPalette
	
	lda	#%00001001
	sta	INIDISP
	
	rts

;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Palette functions
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
LoadShooterPalette:	
	stz 	CGADD		; select address $0 in CG-RAM

	stz	WRCGDATA	
	stz	WRCGDATA	; write black to index

	lda	#0
	sta 	WRCGDATA
	lda	#%11100000
	sta 	WRCGDATA	; write blue to index 1

	lda	#%10000000
	sta 	WRCGDATA
	lda	#%00000011
	sta 	WRCGDATA	; green

	lda	#%11110111
	sta	WRCGDATA
	lda	#%11011110
	sta	WRCGDATA	; gray

	rts
	
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
;;; VRAM functions
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
LoadVRAM:
	; - A:X is the source address for transfer
	; - Y is the number of bytes to copy

	; Save registers
	phb
	php

	stx $4302
	sta $4304
	sty $4305

	lda #$01
	sta $4300 	; DMA mode: word, normal increment
	lda #$18
	sta $4301	; write to VRAM write register
	lda #$01
	sta $420B	; begin DMA transfer on Channel 0

	; Restore registers and return
	plp
	plb

	rts

;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
;;; OAM functions
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
InitializeOAM:
	; Initialize our RAM OAM copy.
	phb
	php

	; Reset the whole OAM to 0.
	ldy	#$200
	lda	#0
@loop:
	sta	(PtrOAM),Y
	dey
	cpy	#0
	bne	@loop

	plp
	plb
	rts

;;;
	
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BSS space.
;;; ;;;;;;;;;;;;;;;;;;;;;;;;;
OAM_RAMCopy:	 .res $200, 0	; $200 byte area for OAM

PlayerPosX:	.byte 0
PlayerPosY:	.byte 0

	.segment "BANK1"
	;; Accessible in Page $01
	;; $018000-$01FFFF == BANK1 $0000-$7FFF

GFXData:
	.incbin	"gfx/gfx.bin"

GFX2bpp:
	.incbin "gfx/gfx2bpp.bin"
