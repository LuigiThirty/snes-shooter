	.macro GoACC8
	sep	#$20	; 8-bit accumulator
	.a8
	.endmacro

	.macro GoACC16
	rep #$20	; 16-bit accumulator
	.a16
	.endmacro

	.macro GoIDX8
	sep	#$10
	.i8
	.endmacro

	.macro GoIDX16
	rep	#$10
	.i16
	.endmacro