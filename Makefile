AS=ca65
LD=ld65

ASFLAGS = -g --cpu 65816
LDFLAGS = -C lorom128.cfg

OBJDIR = obj

all: shooter.smc

$(OBJDIR)/shooter.o: shooter.s strings.s
	$(AS) $(ASFLAGS) -l hello.lst -o $@ $<

shooter.smc: $(OBJDIR)/shooter.o
	$(LD) $(LDFLAGS) -m hello.map -o $@ $^

.phony: all clean run

clean:
	rm $(OBJDIR)/*.o
	rm shooter.smc

run:
	bsnes ./shooter.smc
